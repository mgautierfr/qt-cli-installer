#!/usr/bin/env python3
#
# Copyright (C) 2018 Linus Jahn <lnj@kaidan.im>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import sys
import os
import requests
import xml.etree.ElementTree as ElementTree

if len(sys.argv) < 5:
    print("Usage: {} <qt-version> <host> <target> [<arch>]\n".format(sys.argv[0]))
    print("qt-version:   Qt version in the format of \"5.X.Y\"")
    print("host systems: linux, mac, windows")
    print("targets:      desktop, android, ios")
    print("packages:     Packages to install in the format qt.qt5.5131 qt.qt5.5131.qtwebengine")
    exit(1)

base_url = "https://download.qt.io/online/qtsdkrepository/"

# Qt version
qt_version = sys.argv[1]
qt_ver_num = qt_version.replace(".", "")
# one of: "linux", "mac", "windows"
os_name = sys.argv[2]
# one of: "desktop", "android", "ios"
target = sys.argv[3]
packages = sys.argv[4:]

# Target architectures:
#
# linux/desktop:   "gcc_64"
# mac/desktop:     "clang_64"
# mac/ios:         "ios"
# windows/desktop: "win64_msvc2017_64", "win64_msvc2015_64",
#                  "win32_msvc2015", "win32_mingw53"
# */android:       "android_x86", "android_armv7"
arch = ""
if os_name == "linux" and target == "desktop":
    arch = "gcc_64"
elif os_name == "mac" and target == "desktop":
    arch = "clang_64"
elif os_name == "mac" and target == "ios":
    arch = "ios"

if arch == "":
    print("We do no support that for now.")
    exit(1)

# Build repo URL
packages_url = base_url
if os_name == "windows":
    packages_url += os_name + "_x86/"
else:
    packages_url += os_name + "_x64/"
packages_url += target + "/"

archives_to_install = []

# Get License packages
reply = requests.get(packages_url + "licenses/Updates.xml")
update_xml = ElementTree.fromstring(reply.content)
for packageupdate in update_xml.findall("PackageUpdate"):
    name = packageupdate.find("Name").text
    display_name = packageupdate.find("DisplayName").text
    full_version = packageupdate.find("Version").text
    if packageupdate.find("DownloadableArchives").text:
        archives = packageupdate.find("DownloadableArchives").text.split(", ")
        archive_urls = [
            "{base_url}licenses/{name}/{full_version}{archive}".format(
                 base_url=packages_url,
                 name=name,
                 full_version=full_version,
                 archive=a) for a in archives]
        archives_to_install.append((display_name, archive_urls))

# Get packages index
packages_url += "qt5_" + qt_ver_num + "/"
update_xml_url = packages_url + "Updates.xml"
reply = requests.get(update_xml_url)
update_xml = ElementTree.fromstring(reply.content)

for packageupdate in update_xml.findall("PackageUpdate"):
    name = packageupdate.find("Name").text
    if name in ("{}.{}".format(p, arch) for p in packages):
        display_name = packageupdate.find("DisplayName").text
        full_version = packageupdate.find("Version").text
        if packageupdate.find("DownloadableArchives").text:
            archives = packageupdate.find("DownloadableArchives").text.split(", ")
            archive_urls = [
                "{base_url}{name}/{full_version}{archive}".format(
                    base_url=packages_url,
                    name=name,
                    full_version=full_version,
                    archive=a) for a in archives]
            archives_to_install.append((display_name, archive_urls))
if not full_version or not archives:
    print("Error while parsing package information!")
    exit(1)


print("****************************************")
print("HOST:      ", os_name)
print("TARGET:    ", target)
print("ARCH:      ", arch)
print("****************************************")

for archive_name, archives_urls in archives_to_install:
    sys.stdout.write("\033[K")
    print("Installing {}...".format(archive_name))
    for url in archives_urls:
        print("Downloading {}...".format(url), end="\r")
        os.system("wget -q -O package.7z " + url)

        sys.stdout.write("\033[K")
        print("Extracting {}...".format(url), end="\r")
        os.system("7z x package.7z 1>/dev/null")
        os.system("rm package.7z")

sys.stdout.write("\033[K")
print("Finished installation")
